---
title: Trackers first-party
---

Depuis le 07 novembre se déroule une nouvelle bataille contre le tracking publicitaire. Au cas où vous seriez passé à côté, vous trouverez un peu de détails [ici](https://reflets.info/articles/quand-libe-cache-les-trackers-publicitaires-sous-le-cyber-tapis), [là](https://reflets.info/articles/liberation-a-traqueur-vaillant-rien-d-impossible) et [là](https://reflets.info/articles/affreux-sales-et-mechants).
Cet article a vocation à éclaircir les choses de manière un peu plus posée et technique.

# Tracker first-party ? Qu’es aquò ?
## Le tracking tiers (third-party)

Quand un site internet veut organiser du tracking utilisateur sur son site, il n’héberge généralement pas directement le système de suivi sur sa propre infrastructure mais passe par un service clef-en-main en prestation, par exemple Google Analytics, ChartBeat ou Xiti.

La méthode habituelle pour intégrer ces systèmes passaient par de l’intégration dite « tierce partie ». Pour faire simple, le site principal demandait à votre navigateur d’aller chercher du contenu sur un **autre** domaine. Le contenu tiers est parfaitement visible et identifié sur cette capture d’écran : tout ce qui est en rouge est du contenu tiers (et ici bloqué par défaut grâce à [µMatrix](https://addons.mozilla.org/fr/firefox/addon/umatrix/)).

![Libération](/assets/images/20191113/liberation.png)

Mais ça, c’était avant…

## Le tracking first-party

Suite à une annonce tonitruante de Libération qui se targuait d’être dorénavant plus blanc que blanc pour ses abonnés avec le retrait de tous les trackers publicitaires, j’ai procédé à une petite analyse de leur site une fois connecté en tant qu’abonné. Et j’ai obtenu sur la capture d’écran que vous avez vu plus haut.

Au premier coup d’œil, on voit déjà que la promesse n’est pas tenue.
On pourrait à la limite concéder que ACMP ou ChartBeat n’est « que » du suivi d’audience et en gros ce qui ramène les subventions publiques (qui sont directement indexées sur leur fréquentation) chez Libération.
Par contre, Google Analytics et Tag Commander (qui n’apparaît pas sur la capture d’écran, trop de trackers pour tout faire tenir sur une page…), on sort assez rapidement du pur suivi de fréquentation pour glisser franchement dans <s>le marketing</s>, pardon, la publicité…

Et d’un coup, un truc a commencé à me chafouiner…

Autant `media`, `statics` et `www`, je voyais plus ou moins à quoi ça pouvait correspondre et servir. Mais `f7ds` ? Allons faire un petit tour dans la console réseau de Firefox…

![Eulerian](/assets/images/20191113/eulerian-1.png)

![Eulerian](/assets/images/20191113/eulerian-2.png)

Hum, ça ne ressemble pas à grand-chose que je connaisse, et en tout cas ce n’est pas du contenu mais plutôt des informations qui partent à l’extérieur (résolution d’écran, nombre d’articles lus, durée des sessions…)

Voyons voir le DNS…

    $ dig +short f7ds.liberation.fr
    liberation.eulerian.net.
    atc.eulerian.net.
    109.232.197.179

Et boom… Eulerian… Dixit leur communication « Basée à Paris, Eulerian Technologies est une entreprise innovante française qui a, depuis 2002, construit une technologie de collecte de data e-marketing, qui permet d’analyser en temps réel des milliards de données ». Du bon bullshit comme on les aime ! Et bien entendu, pas de publicité hein… du « data e-marketing » SVP !

Ce type de tracking est appelé « first-party ». À l’inverse du contenu 3rd-party qui est hébergé sur un domaine différent du site principal, le tracking 1st-party provient directement du site visité lui-même.

# Et alors, qu’est-ce que ça change en fait ?

Et bien tout…

## Pour les outils de blocage

Le fonctionnement par contenu tiers a un gros avantage : il est visible ! Non seulement par les utilisateurs, qui avec des outils peuvent l’identifier rapidement, mais aussi par les outils de blocage, qui peuvent du coup maintenir à jour [des listes de blocages](https://github.com/StevenBlack/hosts/blob/c999983bee21eaff6f902477f8330e56bba6a5bb/data/adaway.org/hosts#L514).
L’usage d’un service donné étant caractérisé par le même domaine quel que soit le site visité (Google Analytics reste `google-analytics.com` que vous soyez sur `lemonde.fr` ou `liberation.fr`), ces blocages sont extrêmement efficaces puisque globaux. Le détecter sur un site une unique fois et ajouter à une liste une seul fois protègera l’utilisateur sur tous les sites.
Ce tracking first-party passe du coup complètement sous le radar puisqu’il est assimilé au contenu principal.

Le travail de blocage est beaucoup plus difficile que pour les 3rd-parties : on ne sait pas facilement dire « bloque tout eulerian.net ».
En effet, tout se joue au niveau DNS et en particulier au niveau de la résolution du nom de domaine.
Le client qui cherche à résoudre `f7ds.liberation.net` va demander à votre résolveur DNS (généralement celui de votre fournisseur d’accès) « Eh, pourrais-tu me donner l’IP correspondante ? » et va voir en réponse « C’est `109.232.197.179` ».
À aucun moment le client n’est informé que sur le chemin se trouve un `liberation.eulerian.net`…
C’est uniquement en interne que **le résolveur DNS** va faire le parcours `f7ds.liberation.fr` alias `liberation.eulerian.net` alias `atc.eulerian.net` alias `109.232.197.179`.
Et les outils de blocage n’ont du coup pas accès à cette information…

Ils ont par contre [déjà commencé à réfléchir à des moyens d’action](https://github.com/uBlockOrigin/uBlock-issues/issues/780), mais les pistes actuelles sont peu engageantes.
Les navigateur comme Chrome ne proposent a priori pas les API nécessaires.
Côté Firefox on a le nécessaire, mais il va être nécessaire de refaire côté extension la résolution DNS faite côté navigateur. Même si du coup on devrait profiter à plein des systèmes de caches, le côté perte de performance pourrait être notable.

Si on veut réutiliser les outils existants, il va falloir identifier non plus les domaines finaux (`eulerian.net`) mais le domaine initial (`f7ds.liberation.fr`).
Idem, des outils [commencent à voir le jour](https://git.frogeye.fr/geoffrey/eulaurarien/) pour analyser dynamiquement des listes de sites pour détecter les trackers et pouvoir les bloquer par liste comme auparavant.

En bref, tout l’ancien boulot réalisé sur le 3rd-party est à refaire pour le 1st-party… Trouver des moyens de détection, identifier les domaines en question, remplir des listes de blocage…
Et tout prend des proportions une échelle au-dessus de ce qu’on connaissait côté 3rd-party.
Les utilisateurs habituels de solution de blocage risquent fort de ne pas voir arriver de solution pérenne avant plusieurs mois voire années…

Pour noircir encore un peu plus le tableau, les nouveaux blocages sont beaucoup plus fragiles qu’en 3rd-party.
Il suffira à un site de modifier ses domaines traçants, passer de `f7ds` à `phai` voire `securite` (Ne rigolez pas [01net l’a fait](https://twitter.com/aeris22/status/1193644687950860289)…), mettre des CNAME anonyme ([déjà constaté](https://github.com/uBlockOrigin/uBlock-issues/issues/780#issuecomment-552891839) sur 20minutes) ou par la suite passer directement d’un CNAME à un A/AAAA (la référence au prestataire disparaissant donc).

## Pour la sécurité

Le fait que les trackers 3rd-party soient par définition sur un domaine qui n’est pas le domaine principal apporte aussi des protections au niveau du navigateur, via les limitations dites « [Same Origin Policy](https://developer.mozilla.org/fr/docs/Web/Security/Same_origin_policy_for_JavaScript) ».
L’hypothèse faite par les navigateurs est que si du contenu d’un sous-domaine du domaine principal, alors il est légitime et a globalement les mêmes droits que ce qui vient du domaine principal lui-même.
Il a par exemple accès à vos cookies, peut exécuter du javascript ou accéder au contenu d’une iframe.

Idem, le « [Content Security Policy](https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Content-Security-Policy) » est mis à mal. Étant donné qu’il est rapidement compliqué de faire des règles très strictes, la plupart des sites généralistes se contentent de limiter la casse en bloquant tout sauf leur propre domaine. Et laissent du coup passer les 1st-parties avec, qui se retrouvent à nouveau avec les pleins pouvoirs…

![Boursorama](/assets/images/20191113/boursorama.png)

Libération s’est pris les pieds dans le tapis à ce niveau. Leur cookie d’authentification `djazsession` fuite sur la requête vers Eulerian.
N’importe qui là-bas peut donc récupérer ce cookie, l’intégrer à son navigateur via la console de développement et il est devenu vous, avec accès à votre compte client, votre nom, adresse, données bancaires…
Ici on ne parle que d’un cookie et d’un journal, mais on peut imaginer des choses bien pires, par exemple une banque, sur une page de login, avec une régie pub verrolée qui enverrait du javascript pour relire votre identifiant bancaire…
Ne rigolez pas trop, Boursorama [court à la catastrophe](https://twitter.com/aeris22/status/1194213814901858305) avec ses 1st-parties…

## Pour la réglementation

Toutes ces maniguances n’ont en réalité qu’un seul but : violer en long, en large et en travers le RGPD.
Ces entreprises font tout pour éviter ce qu’elles redoutent le plus au monde, à savoir avoir à demander votre consentement pour pouvoir vous tracker.

Oui, parce que leur collecte est bien entendue complètement illégale…
Et qu’ils savent bien que s’ils doivent réclamer le consentement des visiteurs, qui plus est en opt-in (donc sans action pas de tracking), c’est quasiment l’intégralité de leur trafic, et donc de leur business, qui disparaît instantanément…

Ils ont beau dire « c’est du marketing et non de la publicité », avoir découpé les bonnes anciennes régies pub en DMP/DSP/SSP/XChange/mCRM/trading-desk & j’en passe, ça n’en reste pas moins exactement les mêmes objectifs et données collectées qu’il y a 10 ans. Voire [beaucoup plus](https://iq.opengenus.org/audio-fingerprinting/), et bien plus précises.

<figure>
	<img src="/assets/images/20191113/ad-ecosystem.png" alt="Écosystème de la publicité" />
	<figcaption>
		Écosystème du marché de la publicité
	</figcaption>
</figure>

Les règles du jeu sont pourtant très claires : l’entrée en vigueur du RGPD depuis mai 2018 a quasiment rendu le consentement et l’opt-in (vous n’êtes pas tracké par défaut, vous devez faire une action spécifique pour vous retrouver tracker) obligatoire, alors que le régime de l’opt-out (vous êtes tracké par défaut, vous devez faire une action spécifique pour arrêter d’être tracker) prévalait auparavant.

C’est d’autant plus vrai dans le cadre du suivi d’audience, qui est le cas favori des 1st-party du moment, où [les lignes directrices WP 247](https://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=610140) sur l’application du RGPD sont claires et précises (page 18).
Dès lors que vous faites un suivi des visiteurs (quels articles a lus untel ou quels mots-clefs intéressent untel autre) et non de votre contenu (combien de personnes a vu tel ou tel contenu), vous êtes sous le régime du consentement et non de l’intérêt légitime.
Dès lors que vous externalisez ce suivi à un tiers, les restrictions sont draconiennes pour ne pas avoir à appliquer le régime du consentement.

Dans tous les cas observés aujourd’hui, ni l’une ni l’autre des conditions ne sont remplies et donc le régime du consentement et de l’opt-in s’applique. Le suivi est associé à un utilisateur, ne serait-ce que par la dépose d’un cookie spécifique au visiteur et collecte des données extrêmement personnelles (résolution d’écran, user-agent…), ce qui fait que l’hypothèse d’un pur suivi de contenu ne tient pas.

Dans le cas précis d’Eulerian, le CEO m’a écrit en personne qu’ils se servaient des données collectées pour recroiser avec le service des ventes.

![CEO Eulerian](/assets/images/20191113/backoffice.png)

Les données collectées sont aussi en contradiction avec le WP 247 dans le cas de collecte par un tiers.
Les IP ne sont ainsi pas anonymisées puisque l’accès au service en 1st-party se fait directement par le visiteur, c’est donc by-design que l’adresse IP complète file vers la collecte.
Au moins via le recroisement avec l’après-vente, le cookie identifiant sert à d’autres finalités que la mesure d’audience et permet de remonter à une vente.
L’opt-out n’est pas possible, et même lorsque le 1st-party en propose une, il s’agit généralement uniquement d’un opt-out pour les trackers en 3rd-party. Il ne peut de toute façon pas en être autrement, puisqu’un éventuel cookie d’opt-out déposé sur `eulerian.net` ne circule pas, lui, quand la ressource est visitée depuis `f7ds.liberation.fr`.

On sait pourtant faire les choses proprement, comme le prouve [NextInpact](https://www.nextinpact.com/) avec son [Matomo](https://matomo.org/) auto-hébergé [en propre](https://tim.nextinpact.com/) et configuré selon [les exigences de la CNIL](https://www.cnil.fr/sites/default/files/typo/document/Configuration_piwik.pdf).

# Comment se protéger ?

Eh bien c’est là que ça se gâte…
Actuellement il n’existe pas de moyen simple et accessible de bloquer ce type de tracker…

Les outils type [µBlock](https://addons.mozilla.org/fr/firefox/addon/ublock-origin/) ne savent actuellement pas traiter le cas sinon devoir lister explicitement tous les domaines 1st-party. Le boulot est en cours mais ça va mettre un moment et comme dit précédement, c’est mort d’avance pour Chrome.

La solution d’utiliser [µMatrix](https://addons.mozilla.org/fr/firefox/addon/umatrix/) en mode paranoïa ie bloquer tout ce qui n’est pas issu directement du domaine visité est inutilisable, 99% d’Internet devenant immédiatement tout blanc et quasiment plus aucun site ne fonctionnant correctement.
Ça a par contre l’avantage de prendre conscience de l’enfer qu’est devenu le web…

Une solution plus efficace est d’utiliser son propre résolveur DNS plutôt que de dépendre de celui de son FAI. Shaft a commencé à proposer un bout de solution [ici](https://framagit.org/Shaft/blocage-dmp-unbound).
Pour la très grosse majorité des gens, c’est aussi une solution à peu près inutilisable encore une fois.

Du travail est aussi en cours du côté de [Pi-hole](https://pi-hole.net/), qui est éventuellement plus accessible pour le grand public que la solution précédente, mais reste quand même assez peu fréquente aujourd’hui.

Sur mobile, on n’en parle même pas, cet environnement risque d’être sans solution… On prie pour que [Blokada](https://blokada.org/) trouve comment détecter les CNAME.

Pas grand chose de très efficace et encore moins pour le grand public donc…
Tout ça reste bien plus complexe que les anciennes méthodes qui ne réclamaient que l’installation d’une extension de navigateur accessible à tous…

De biens jolies saloperies en perspective…
